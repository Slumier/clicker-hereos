package fr.home.mouse;

import java.awt.*;
import java.awt.event.InputEvent;
import java.util.HashMap;

public class MouseAction {

    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    public void mouseClicked( Robot rbt ) {
        rbt.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        rbt.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    }

    public void mouseWhereClicked(Robot rbt, int x, int y) {
        rbt.mouseMove(x, y);
        mouseClicked( rbt );
    }


    /**
     * int i -> width = 4
     * int j -> height = 6
     *
     * ___SCREEN____
     * i j j j j j j
     * i j j j j j j
     * i j j j j j j
     * i j j j j j j
     */
    public void getPixelColorInterval() {

        //get the center point of screen : (x,y)
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
        Double convertX = new Double(width/2 );
        Double convertY = new Double(height/2 );
        int middleX = convertX.intValue();
        int middleY = convertY.intValue();

    }

}
