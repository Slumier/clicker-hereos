package fr.home.tools;

import java.awt.*;
import java.util.HashMap;

public class Tools {

    public static void printMapValue ( HashMap map ) {
        System.out.println( "==================" );
        System.out.println( "map size : " + map.size() );
        System.out.println( "==================" );
        for (Color keyStr : (Iterable<Color>) map.keySet()) {
            System.out.println( map.get(keyStr) + "" );
        }
        System.out.println( "==================" );
    }

    public static void printMapKey ( HashMap map ) {
        System.out.println( "==================" );
        System.out.println( "map size : " + map.size() );
        System.out.println( "==================" );
        for (Color keyStr : (Iterable<Color>) map.keySet()) {
            System.out.println( keyStr + "" );
        }
        System.out.println( "==================" );
    }

    public static int convertDoubleToInteger( Double var ) {

        Double convert = new Double( var );
        return convert.intValue();

    }
}
